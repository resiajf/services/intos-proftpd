FROM debian:stretch

RUN apt-get update -y && \
    apt-get install -y proftpd curl && \
    apt-get clean && \
    rm -r /var/lib/apt && \
    rm -r /var/cache/apt

COPY start.sh /usr/bin/proftpd-start.sh
COPY proftpd.conf /etc/proftpd/proftpd.conf

ENV PROFTPD_SERVERNAME proftpd
#ENV PROFTPD_UID 1000 #Not needed to override
#ENV PROFTPD_GID 1000 #Not needed to override
ENV PROFTPD_PASSIVE_MIN 50000
ENV PROFTPD_PASSIVE_MAX 51000
ENV PROFTPD_HOMEDIR /home/ftp

EXPOSE 20 21

CMD proftpd-start.sh
