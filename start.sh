#!/bin/bash -e

echo "$USERNAME - $PASSWORD"

#Take current UID/GID
MOUNTUID=$(stat -c '%u' "$PROFTPD_HOMEDIR")
MOUNTGID=$(stat -c '%g' "$PROFTPD_HOMEDIR")

OVERRIDEUID=$MOUNTUID
OVERRIDEGID=$MOUNTGID

if [ -n "$PROFTPD_UID" ]; then
    OVERRIDEUID=$PROFTPD_UID
    OVERRIDEGID=$PROFTPD_UID

    [ -n "$PROFTPD_GID" ] && OVERRIDEGID=$PROFTPD_GID
fi

printf "$PASSWORD" | ftpasswd --passwd --file=/etc/proftpd/passwd --name=$USERNAME \
    --uid=$OVERRIDEUID --gid=$OVERRIDEGID --home=$PROFTPD_HOMEDIR \
    --shell=/bin/sh --stdin

exec proftpd --nodaemon $PROFTPD_ARGS
